# Rusk

Rusk is a robust frontend application core written in the Rust language. It can compile into native applications as well as WebAssembly modules. It utilizes the eGui library for GUI creation, the Rayon library for multithreading (even on WebAssembly, thanks to wasm-bindgen-rayon), and the Specs entity framework to integrate various components seamlessly.

Additionally, it leverages the innovative wgpu API to unlock more powerful rendering capabilities when deployed on the web.

## Structure
#### Application
The Application module serves as the project's main component. Other modules play supporting roles. It encompasses the shared code for both the wasm and native versions, encompassing GUI logic, application functionality, and the Specs system.

#### dev_server
A minimal implementation of a server designed to serve compiled WebAssembly modules on localhost.

#### gentest_api
An illustration of OpenAPI-generated models and APIs, which are employed in the current GUI implementation showcase.

#### shared
The shared module contains the necessary code to relay UI events from the main browser thread to the web worker (see Caveats).

#### web_main
This module implements a WebAssembly module that runs on the primary browser thread, forwarding all events to the web worker responsible for executing the Application module.

#### wgpu-hal and egui_wgpu_backend
Local copies of these libraries were modified to make eGUI compatible with webGPU when operating within the web worker.

## GUI
The application demonstrates how an immediate GUI library can cooperate with the Specs Entity Component System, creating an extensible and maintainable framework. The concept involves using a System for each distinct GUI element (such as a window with a list of project names), while storing the associated data in the Specs Storage system to adhere to ECS design patterns. Subsequently, each GUI system requests access to the requisite Storages for GUI rendering. While this approach segregates data from the GUI, which might appear counterintuitive to the essence of immediate GUIs, it avoids data duplication. Addressing data duplication is a primary concern that immediate GUIs address, as their typical blend of application logic and GUI code can become unwieldy and cumbersome. This project illustrates a sensible integration of immediate GUIs and Entity Component Systems, which scales well for sizeable GUI-driven applications.

## OpenAPI
The project is ready to integrate with auto-generated OpenAPI models and APIs. The current demonstration showcases a straightforward Project model (comprising only a string field for the Name), with provisions already in place for asynchronous data retrieval from generated endpoints. The "projects_window.rs" file demonstrates the implementation of a GUI window that fetches a list of projects asynchronously. This involves a Fetcher class (defined in fetcher.rs) that returns a Promise for the fetched data. The implementation employs the wasm_bindgen_futures library for WebAssembly and the tokio framework for the native clientto fulfill the Promise.


## Caveats
To enable multithreading in WebAssembly, a significant portion of the application operates within a web worker, including the GUI. The wasm build yields two modules: one for the main browser thread, responsible for transferring data to the web worker (implemented in the web_main module), and another for the application module containing the entirety of the application code. This approach is essential to prevent any blocking within the main browser thread, while utilizing Rayon library for multithreading.

#### Disclaimer: This is a legacy project that hasn't received updates for several years. Modifications made at that time were necessary to achieve functionality and may no longer be required. Notably, local modifications to the egui_wgpu_backend and wgpu-hal libraries were performed to enable webGPU compatibility with eGUI.