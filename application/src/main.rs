#![feature(fn_traits)]
#![feature(trait_alias)]

use winit::event::Event::{RedrawRequested, WindowEvent};
use winit::event_loop::ControlFlow;

pub mod application;
pub mod shared;

const INITIAL_WIDTH: u32 = 1920;
const INITIAL_HEIGHT: u32 = 1080;

// When compiling natively:
#[cfg(not(target_arch = "wasm32"))]
#[tokio::main]
async fn main() {
    simple_logger::SimpleLogger::new().init().unwrap();
    let event_loop = winit::event_loop::EventLoop::with_user_event();
    let window = winit::window::WindowBuilder::new()
        .with_decorations(true)
        .with_resizable(true)
        .with_transparent(false)
        .with_title("Rusk")
        .with_inner_size(winit::dpi::PhysicalSize {
            width: INITIAL_WIDTH,
            height: INITIAL_HEIGHT,
        })
        .build(&event_loop)
        .unwrap();

    let mut test = futures::executor::block_on(application::Application::new(
        &window,
        &(),
        window.inner_size(),
        window.scale_factor() as f32,
    ));

    event_loop.run(move |event: shared::WEvent, _, control_flow| {
        test.handle_event(&event);
        match event {
            RedrawRequested(..) => {
                *control_flow = ControlFlow::Poll;
            }
            shared::WEvent::MainEventsCleared => {
                window.request_redraw();
            }
            WindowEvent { event, .. } => match event {
                winit::event::WindowEvent::CloseRequested => {
                    *control_flow = ControlFlow::Exit;
                }
                _ => {}
            },
            _ => (),
        }
    });
}
