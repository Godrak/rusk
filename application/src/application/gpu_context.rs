use raw_window_handle;
use winit::dpi::PhysicalSize;

#[cfg(not(target_arch = "wasm32"))]
pub type OffCanvas = ();
#[cfg(target_arch = "wasm32")]
pub type OffCanvas = web_sys::OffscreenCanvas;

pub struct GpuContextRes {
    pub device: wgpu::Device,
    pub queue: wgpu::Queue,
    pub surface: wgpu::Surface,
    pub surface_config: wgpu::SurfaceConfiguration,
    pub scale_factor: f32,
}

// Note: the Instance, Surface, PhysicalDevice, Device, and Queue can only have their methods called on the thread where Instance was created(!).
// Recording command buffers is free-threaded.
impl GpuContextRes {
    pub async fn new<WHandle: raw_window_handle::HasRawWindowHandle>(
        window_size: PhysicalSize<u32>,
        scale_factor: f32,
        window_handle: &WHandle,
        _offscreen_canvas: &OffCanvas, //TODO preparation for webgpu offscreen support
    ) -> Self {
        let instance: wgpu::Instance;
        let surface: wgpu::Surface;
        let limits: wgpu::Limits;
        #[cfg(target_arch = "wasm32")]
        {
            instance = wgpu::Instance::new(wgpu::Backends::GL);
            surface = unsafe { instance.create_surface(window_handle) };
            limits = wgpu::Limits::downlevel_webgl2_defaults();
        }
        #[cfg(not(target_arch = "wasm32"))]
        {
            instance = wgpu::Instance::new(wgpu::Backends::GL);
            surface = unsafe { instance.create_surface(window_handle) };
            limits = wgpu::Limits::default();
        }

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                // Request an adapter which can render to our surface
                force_fallback_adapter: false,
                compatible_surface: Some(&surface),
            })
            .await
            .expect("Failed to find an appropriate adapter");

        // Create the logical device and command queue
        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: None,
                    features: wgpu::Features::default(),
                    limits,
                },
                None,
            )
            .await
            .expect("Failed to create device");

        let surface_format = surface.get_preferred_format(&adapter).unwrap();
        let surface_config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface_format,
            width: window_size.width,
            height: window_size.height,
            present_mode: wgpu::PresentMode::Mailbox,
        };
        surface.configure(&device, &surface_config);

        GpuContextRes {
            device,
            queue,
            surface,
            surface_config,
            scale_factor,
        }
    }

    pub fn resize(&mut self, size: &PhysicalSize<u32>) {
        self.surface_config.width = size.width;
        self.surface_config.height = size.height;
        self.surface.configure(&self.device, &self.surface_config);
    }

    pub fn new_scale_factor(&mut self, scale_factor: f32) {
        self.scale_factor = scale_factor;
    }
}
