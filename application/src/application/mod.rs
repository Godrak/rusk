pub use super::shared::*;
use raw_window_handle::HasRawWindowHandle;
use specs::prelude::*;
use winit::dpi::{LogicalSize, PhysicalSize};

mod core_data;
mod gpu_context;
mod gentest;
mod gui;

use egui::Event;
use gpu_context::{GpuContextRes, OffCanvas};
use winit::window::WindowId;
use winit::event::WindowEvent;

pub struct Application {
    world: World,
    dispatcher: Dispatcher<'static, 'static>,
}

impl Application {
    pub async fn new<WHandle: HasRawWindowHandle>(
        window_handle: &WHandle,
        off_canvas: &OffCanvas,
        size: PhysicalSize<u32>,
        scale_factor: f32,
    ) -> Self {
        let mut world = World::new();
        //*********************************
        // INSERT RESOURCES
        //*********************************
        {
            let gpu_res =
                gpu_context::GpuContextRes::new(size, scale_factor, window_handle, off_canvas)
                    .await;
            let gui_res = gui::GuiFrameRes::new(&gpu_res);
            world.insert(gpu_res);
            world.insert(gui_res);

            world.insert(core_data::TimerRes::default());
            world.insert(core_data::LoginDataRes::default());
            world.insert(core_data::Fetcher::new());

            world.insert(gui::GuiContextRes::default());
            world.insert(gui::GlobalMenu::default());
            world.insert(gui::ActiveScreen::default());
            world.insert(gui::WindowCreatorRes::default());
        }
        //*********************************
        // REGISTER COMPONENTS
        //*********************************
        {
            world.register::<gui::Screen>();
            world.register::<gui::LoginWindow>();
            world.register::<gui::GuiSettingsWindow>();
            world.register::<gui::ProjectsWindow>();
            world.register::<gui::ProjectWindow>();
        }

        //*********************************
        //BUILD DISPATCHER
        //NOTE: gui start frame system, gui end frame system and other rendering systems should be on main thread only
        //         If it is necessary to execute parallel dispatch, use with_thread_local for those systems instead.
        //*********************************
        let dispatcher = DispatcherBuilder::new()
            .with(gui::GuiStartFrameSystem::default(), "gui_start_frame", &[])
            .with(gui::WindowCreatorSystem::default(), "window_creator", &["gui_start_frame"])
            .with(gui::GlobalMenuSystem::default(), "global_menu", &["gui_start_frame"])
            .with(gui::LoginWindowSystem::default(), "login_windows", &["gui_start_frame"])
            .with(gui::GuiSettingsWindowSystem::default(), "gui_setting_windows", &["gui_start_frame"])
            .with(gui::ProjectsWindowSystem::default(), "projects_windows", &["gui_start_frame"])
            .with(gui::ProjectWindowSystem::default(), "project_windows", &["gui_start_frame"])
            .with_barrier() //barrier for ui systems
            .with(gui::GuiEndFrameSystem::default(), "gui_end_frame", &["gui_start_frame"])
            .build();

        Application { world, dispatcher }
    }

    pub fn handle_event(&mut self, event: &WEvent) {
        self.world
            .get_mut::<core_data::TimerRes>()
            .unwrap()
            .start_frame();
        self.world
            .get_mut::<gui::GuiFrameRes>()
            .unwrap()
            .handle_event(event);

        match event {
            WEvent::RedrawRequested(..) => {
                self.dispatcher.dispatch(&self.world);
                self.world.maintain();
            }

            WEvent::UserEvent(CustomEvent::Paste(text)) => {
                for char in text {
                    unsafe {
                        self.world
                            .get_mut::<gui::GuiFrameRes>()
                            .unwrap()
                            .handle_event(&WEvent::WindowEvent {
                                event: WindowEvent::ReceivedCharacter(char::from(char.clone())),
                                window_id: WindowId::dummy(),
                            })
                    }
                }
            }
            WEvent::WindowEvent { event, .. } => match event {
                winit::event::WindowEvent::Resized(size) => {
                    let gpu_context = self.world.get_mut::<GpuContextRes>().unwrap();
                    gpu_context.resize(size);
                }
                winit::event::WindowEvent::ScaleFactorChanged {
                    scale_factor,
                    new_inner_size,
                } => {
                    log::info!(
                        "new size: width: {} and height: {}",
                        new_inner_size.width,
                        new_inner_size.height
                    );
                    self.world
                        .get_mut::<GpuContextRes>()
                        .unwrap()
                        .new_scale_factor(scale_factor.clone() as f32);
                    self.world
                        .get_mut::<GpuContextRes>()
                        .unwrap()
                        .resize(new_inner_size);
                }
                winit::event::WindowEvent::CloseRequested => {}
                _ => {}
            },
            _ => (),
        }
    }
}
