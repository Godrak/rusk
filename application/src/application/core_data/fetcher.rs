use std::sync::{Arc, Mutex};
use crate::application::gentest;
use std::fmt::Debug;
use std::thread::sleep;
use instant::Duration;
use futures::Future;
use futures::future::{BoxFuture, LocalBoxFuture};
use crate::application::gentest::Configuration;
use std::any::Any;

#[cfg(not(target_arch = "wasm32"))]
pub trait NativeSend = Send;

#[cfg(not(target_arch = "wasm32"))]
type BoxedF<'a, T> = BoxFuture<'a, T>;

#[cfg(target_arch = "wasm32")]
pub trait NativeSend = Any;

#[cfg(target_arch = "wasm32")]
type BoxedF<'a, T> = LocalBoxFuture<'a, T>;


pub struct FetchPromise<T: Clone> {
    data: Arc<Mutex<Option<Result<T, String>>>>,
}

impl<T: Clone> FetchPromise<T> {
    pub fn is_done(&self) -> bool {
        self.data.try_lock().map_or(false, |o| o.is_some())
    }

    pub fn get_result(&self) -> Option<Result<T, String>> {
        self.data.lock().unwrap().clone()
    }
}

#[derive(Default)]
pub struct Fetcher {
    cfg: gentest::Configuration,
}

impl Fetcher {
    pub fn new() -> Self {
        Fetcher {
            cfg: gentest::Configuration::new()
        }
    }

    #[cfg(target_arch = "wasm32")]
    fn spawn_task(&self, f: impl Future<Output=()> + 'static) {
        wasm_bindgen_futures::spawn_local(f);
    }

    #[cfg(not(target_arch = "wasm32"))]
    fn spawn_task(&self, f: impl Future<Output=()> + 'static +NativeSend) {
        tokio::spawn(f);
    }

    pub fn create_task<T: 'static + Clone +NativeSend, E: Debug +NativeSend, F: Clone + 'static>(&self,
                                                                         f: &F,
                                                                         after: Duration) -> FetchPromise<T>
        where
            F: for<'a> FnOnce(&'a Configuration, &'a mut Vec<String>) -> BoxedF<'a, Result<T, E>> +NativeSend
    {
        let data = Arc::new(Mutex::new(None));
        let promise: FetchPromise<T> = FetchPromise { data: data.clone() };
        let cfg = self.cfg.clone();
        let task_function = f.clone();
        let mut strvec: Vec<String> = vec![];
        self.spawn_task(async move {
            let res: Result<T, E>;
            sleep(after);
            res = task_function.call_once((&cfg, &mut strvec)).await;
            *data.lock().unwrap() = Some(res.map_err(|e| format!("{:?}", e)));
        });

        promise
    }
}
