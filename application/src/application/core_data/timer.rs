use instant::Instant;

pub struct TimerRes {
    pub start: Instant,
    pub delta: f64,
    prev_time: f64,
}

impl Default for TimerRes {
    fn default() -> Self {
        TimerRes {
            start: Instant::now(),
            delta: 0.0,
            prev_time: 0.0,
        }
    }
}

impl TimerRes {
    pub fn start_frame(&mut self) -> f64 {
        let current = self.start.elapsed().as_secs_f64();
        self.delta = current - self.prev_time;
        self.prev_time = current;
        current
    }

    pub fn frame_time(&self) -> f64 {
        return self.start.elapsed().as_secs_f64() - self.prev_time;
    }
}
