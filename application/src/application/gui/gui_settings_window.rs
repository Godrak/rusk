use super::{show_windows, ScreenWindow, WindowSystemData};
use egui::{Id};
use specs::prelude::*;
use specs::storage::BTreeStorage;

pub struct GuiSettingsWindow {
    entity: Entity,
}

impl Component for GuiSettingsWindow {
    type Storage = BTreeStorage<Self>;
}

impl GuiSettingsWindow {
    pub fn new(entity: Entity) -> Self {
        GuiSettingsWindow { entity }
    }

    pub fn name(&self) -> &'static str {
        "🔧 GUI Settings"
    }
}

impl ScreenWindow<'_> for GuiSettingsWindow {
    type ScreenWindowTypeData = ();

    /// Show ui and return whether the window was closed (and thus entity should be deleted)
    fn ui_show(&mut self, ctx: &egui::CtxRef, _data: &mut Self::ScreenWindowTypeData) -> bool {
        let mut open: bool = true;
        egui::Window::new(self.name())
            .id(Id::new(self.entity))
            .open(&mut open)
            .resizable(true)
            .show(ctx, |ui| {
                ctx.settings_ui(ui);
            });
        open
    }
}

#[derive(Default)]
pub struct GuiSettingsWindowSystem;

impl<'a> System<'a> for GuiSettingsWindowSystem {
    type SystemData = (
        Read<'a, super::GuiContextRes>,
        WindowSystemData<'a>,
        WriteStorage<'a, super::GuiSettingsWindow>,
    );

    fn run(&mut self, (context, window_data, gui_windows): Self::SystemData) {
        let ctx_ref = context.0.as_ref().unwrap();
        show_windows(ctx_ref, window_data, gui_windows, ());
    }
}
