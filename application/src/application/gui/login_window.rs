use crate::application::gui::{show_windows, ScreenWindow, WindowSystemData};
use egui::{Id, Ui};
use specs::prelude::*;
use specs::storage::BTreeStorage;

pub struct LoginWindow {
    entity: Entity,
    username_entry: String,
    password_entry: String,
}

impl Component for LoginWindow {
    type Storage = BTreeStorage<Self>;
}

impl LoginWindow {
    pub fn new(entity: Entity) -> Self {
        LoginWindow {
            entity,
            username_entry: String::new(),
            password_entry: String::new(),
        }
    }

    pub fn name(&self) -> &'static str {
        "🗖 LoginWindow"
    }

    fn login_inputs(&mut self, ui: &mut Ui, _data: &mut LoginWindowSystemData) {
        ui.horizontal(|ui| {
            ui.label("Username: ");
            ui.add(egui::TextEdit::singleline(&mut self.username_entry));
        });
        ui.horizontal(|ui| {
            ui.label("Password: ");
            ui.add(password(&mut self.password_entry));
        });
        if ui.button("Login").clicked() {
            //TODO start login procedure
        }
    }
}

impl<'a> ScreenWindow<'a> for LoginWindow {
    type ScreenWindowTypeData = LoginWindowSystemData<'a>;

    /// Show ui and return whether the window was closed (and thus entity should be deleted)
    fn ui_show(&mut self, ctx: &egui::CtxRef, data: &mut Self::ScreenWindowTypeData) -> bool {
        let mut open: bool = true;
        egui::Window::new(self.name())
            .id(Id::new(self.entity))
            .open(&mut open)
            .resizable(true)
            .show(ctx, |ui| {
                self.login_inputs(ui, data);
            });
        open
    }
}

#[derive(SystemData)]
pub struct LoginWindowSystemData<'a> {
    pub login_data: Write<'a, super::super::core_data::LoginDataRes>,
}

#[derive(Default)]
pub struct LoginWindowSystem;

impl<'a> System<'a> for LoginWindowSystem {
    type SystemData = (
        Read<'a, super::GuiContextRes>,
        WindowSystemData<'a>,
        WriteStorage<'a, super::LoginWindow>,
        LoginWindowSystemData<'a>,
    );

    fn run(
        &mut self,
        (context, window_system_data, login_windows, data): Self::SystemData,
    ) {
        let ctx_ref = context.0.as_ref().unwrap();
        show_windows(ctx_ref, window_system_data, login_windows, data);
    }
}

//NOTE: taken from Misc Demos examples (https://emilk.github.io/egui/index.html#demo)
pub fn password_ui(ui: &mut egui::Ui, text: &mut String) -> egui::Response {
    #[derive(Clone, Copy, Default)]
    struct State(bool);
    let id = ui.id().with("show_password");
    let mut plaintext = *ui.memory().id_data_temp.get_or_default::<State>(id);
    let result = ui.with_layout(egui::Layout::right_to_left(), |ui| {
        let response = ui
            .add(egui::SelectableLabel::new(plaintext.0, "👁"))
            .on_hover_text("Show/hide password");
        if response.clicked() {
            plaintext.0 = !plaintext.0;
        }
        let text_edit_size = ui.available_size();

        ui.add_sized(
            text_edit_size,
            egui::TextEdit::singleline(text).password(!plaintext.0),
        );
    });

    ui.memory().id_data_temp.insert(id, plaintext);

    result.response
}

// A wrapper that allows the more idiomatic usage pattern: `ui.add(...)`
/// Password entry field with ability to toggle character hiding.
///
/// ## Example:
/// ``` ignore
/// ui.add(password(&mut password));
/// ```
pub fn password(text: &mut String) -> impl egui::Widget + '_ {
    move |ui: &mut egui::Ui| password_ui(ui, text)
}
