use specs::Entity;
use specs::prelude::*;

pub enum CreateWindowRequest {
    Screen,
    //Note: does not semantically fit here, as it is not window but screen.
    LoginWindow,
    GuiSettingsWindow,
    ProjectsWindow,
    ProjectWindow(String),
}

#[derive(Default)]
pub struct WindowCreatorRes {
    create_requests: Vec<CreateWindowRequest>,
}

impl WindowCreatorRes {
    pub fn create_request(&mut self, request: CreateWindowRequest) {
        self.create_requests.push(request)
    }
}

#[derive(SystemData)]
pub struct WindowCreatorSystemData<'a> {
    pub entities: Entities<'a>,
    pub screens: WriteStorage<'a, super::Screen>,
    pub active_screen: Write<'a, super::ActiveScreen>,
    pub login_windows: WriteStorage<'a, super::LoginWindow>,
    pub login_data: Read<'a, super::super::core_data::LoginDataRes>,
    pub gui_setting_windows: WriteStorage<'a, super::GuiSettingsWindow>,
    pub projects_windows: WriteStorage<'a, super::ProjectsWindow>,
    pub project_windows: WriteStorage<'a, super::ProjectWindow>,
}

#[derive(Default)]
pub struct WindowCreatorSystem;

impl<'a> System<'a> for WindowCreatorSystem {
    type SystemData = (
        Write<'a, WindowCreatorRes>,
        WindowCreatorSystemData<'a>,
    );

    fn run(&mut self, (mut requests, mut data): Self::SystemData) {
        for request in &requests.create_requests {
            // get or create active screen
            let screen_entity = if let Some(screen) = data.active_screen.0 {
                screen
            } else {
                create_new_active_screen(&mut data, "screen".to_string())
            };

            //create new window entity and add it to the screen
            let new_window_entity = data.entities.create();
            data.screens
                .get_mut(screen_entity)
                .unwrap()
                .windows
                .add(new_window_entity.id());

            //create corresponding window
            match request {
                CreateWindowRequest::Screen => {
                    create_new_active_screen(&mut data, "screen".to_string());
                }
                CreateWindowRequest::LoginWindow => {
                    data.login_windows
                        .insert(new_window_entity, super::LoginWindow::new(new_window_entity))
                        .unwrap();
                }
                CreateWindowRequest::GuiSettingsWindow => {
                    data.gui_setting_windows
                        .insert(new_window_entity, super::GuiSettingsWindow::new(new_window_entity))
                        .unwrap();
                }
                CreateWindowRequest::ProjectsWindow => {
                    data.projects_windows
                        .insert(new_window_entity, super::ProjectsWindow::new(new_window_entity))
                        .unwrap();
                }
                CreateWindowRequest::ProjectWindow(project) => {
                    data.project_windows
                        .insert(new_window_entity, super::ProjectWindow::new(new_window_entity, project.clone()))
                        .unwrap();
                }
            }
        }

        requests.create_requests.clear();
    }
}

fn create_new_active_screen(data: &mut WindowCreatorSystemData, screen_title: String) -> Entity {
    let new_screen_entity = data.entities.create();
    data.screens
        .insert(
            new_screen_entity,
            super::Screen {
                title: screen_title,
                windows: BitSet::default(),
                title_edit_mode: false,
            },
        )
        .unwrap();
    data.active_screen.0 = Some(new_screen_entity);
    new_screen_entity
}
