use crate::application::core_data::TimerRes;
use crate::application::gpu_context::GpuContextRes;
use core::iter;
use egui::*;
use egui_wgpu_backend::{RenderPass, ScreenDescriptor};
use egui_winit_platform::*;
use epi::backend::AppOutput;
use epi::{Frame as UIFrame, WebInfo};
use specs::prelude::*;
use std::ops::Deref;

struct Dummy;

impl epi::RepaintSignal for Dummy {
    fn request_repaint(&self) {}
}

pub struct GuiFrameRes {
    platform: Platform,
    render_pass: RenderPass,
    ui_frame_time: Option<f32>,
}

impl GuiFrameRes {
    pub fn new(gpu: &GpuContextRes) -> Self {
        let platform = Platform::new(PlatformDescriptor {
            physical_width: gpu.surface_config.width as u32,
            physical_height: gpu.surface_config.height as u32,
            scale_factor: gpu.scale_factor as f64,
            font_definitions: FontDefinitions::default(),
            style: Default::default(),
        });

        let render_pass = RenderPass::new(&gpu.device, gpu.surface_config.format, 1);

        // let mut fonts = FontDefinitions::default();
        // fonts.font_data.insert(
        //     "my_font".to_owned(),
        //     std::borrow::Cow::Borrowed(include_bytes!("open-dys.ttf")),
        // ); // .ttf and .otf supported
        // fonts
        //     .fonts_for_family
        //     .get_mut(&FontFamily::Proportional)
        //     .unwrap()
        //     .insert(0, "my_font".to_owned());
        // fonts
        //     .fonts_for_family
        //     .get_mut(&FontFamily::Monospace)
        //     .unwrap()
        //     .push("my_font".to_owned());
        //
        // platform.context().set_fonts(fonts);

        GuiFrameRes {
            platform,
            render_pass,
            ui_frame_time: None,
        }
    }

    pub fn handle_event(&mut self, event: &crate::shared::WEvent) {
        self.platform.handle_event(event);
    }

    pub fn begin_ui_frame<'a>(
        &'a mut self,
        scale_factor: f32,
        elapsed_time: f64,
        app_output: &'a mut AppOutput,
    ) -> (UIFrame<'a>, CtxRef) {
        self.platform.begin_frame();
        self.platform.update_time(elapsed_time);

        let frame = epi::backend::FrameBuilder {
            info: epi::IntegrationInfo {
                name: "egui_web",
                web_info: if cfg!(target_arch = "wasm32") {
                    Some(WebInfo {
                        web_location_hash: "localhost".to_string(),
                    })
                } else {
                    None
                },
                prefer_dark_mode: None,
                cpu_usage: self.ui_frame_time,
                native_pixels_per_point: Some(scale_factor),
            },
            tex_allocator: &mut self.render_pass,
            output: app_output,
            repaint_signal: std::sync::Arc::new(Dummy {}),
        }
        .build();

        return (frame, self.platform.context());
    }

    pub fn end_ui_frame(&mut self, gpu: &GpuContextRes, elapsed: f32) {
        // End the UI frame. We could now handle the output and draw the UI with the backend.
        let (_output, paint_commands) = self.platform.end_frame(None);
        let paint_jobs = self.platform.context().tessellate(paint_commands);
        self.ui_frame_time = Some(elapsed);

        let mut encoder = gpu
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("encoder"),
            });

        // Upload all resources for the GPU.
        let screen_descriptor = ScreenDescriptor {
            physical_width: gpu.surface_config.width,
            physical_height: gpu.surface_config.height,
            scale_factor: gpu.scale_factor as f32,
        };

        self.render_pass.update_texture(
            &gpu.device,
            &gpu.queue,
            &self.platform.context().texture(),
        );
        self.render_pass
            .update_user_textures(&gpu.device, &gpu.queue);
        self.render_pass
            .update_buffers(&gpu.device, &gpu.queue, &paint_jobs, &screen_descriptor);

        let output_frame = match gpu.surface.get_current_texture() {
            Ok(frame) => frame,
            Err(e) => {
                log::warn!("Dropped frame with error: {}", e);
                return;
            }
        };

        let output_view = output_frame
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        // Record all render passes.
        self.render_pass.execute(
            &mut encoder,
            &output_view,
            &paint_jobs,
            &screen_descriptor,
            Some(wgpu::Color::BLACK),
        ).unwrap();

        // Submit the commands.
        gpu.queue.submit(iter::once(encoder.finish()));

        // Redraw egui
        output_frame.present();
    }
}

#[derive(Default)]
pub struct GuiContextRes(pub Option<CtxRef>);

#[derive(Default)]
pub struct GuiStartFrameSystem;

impl<'a> System<'a> for GuiStartFrameSystem {
    type SystemData = (
        WriteExpect<'a, GuiFrameRes>,
        ReadExpect<'a, GpuContextRes>,
        Write<'a, GuiContextRes>,
        Read<'a, TimerRes>,
    );

    fn run(
        &mut self,
        (mut gui_frame_res, gpu_context_res, mut gui_context_res, timer): Self::SystemData,
    ) {
        let mut app_output = epi::backend::AppOutput::default();
        let (mut _ui_frame, ui_context) = gui_frame_res.begin_ui_frame(
            gpu_context_res.scale_factor,
            timer.start.elapsed().as_secs_f64(),
            &mut app_output,
        );

        gui_context_res.0 = Some(ui_context);
    }
}

#[derive(Default)]
pub struct GuiEndFrameSystem;

impl<'a> System<'a> for GuiEndFrameSystem {
    type SystemData = (
        WriteExpect<'a, GuiFrameRes>,
        ReadExpect<'a, GpuContextRes>,
        Write<'a, GuiContextRes>,
        Read<'a, TimerRes>,
    );

    fn run(
        &mut self,
        (mut gui_frame_res, gpu_context_res, mut gui_context_res, timer): Self::SystemData,
    ) {
        gui_frame_res.end_ui_frame(gpu_context_res.deref(), timer.frame_time() as f32);
        gui_context_res.0 = None; //remove context, so that no other ui system can run after this, which would be invalid.
    }
}
