use egui::CtxRef;
use specs::prelude::*;
use std::ops::{BitAndAssign, Not};

#[derive(SystemData)]
pub struct WindowSystemData<'a> {
    pub entities: Entities<'a>,
    pub screens: WriteStorage<'a, super::Screen>,
    pub active_screen: Read<'a, super::ActiveScreen>,
}

pub trait ScreenWindow<'a> {
    type ScreenWindowTypeData;
    fn ui_show(&mut self, ctx: &egui::CtxRef, data: &mut Self::ScreenWindowTypeData) -> bool;
}

pub fn show_windows<'a, T: ScreenWindow<'a> + Component>(
    ctx_ref: &CtxRef,
    window_system_data: WindowSystemData,
    mut windows_storage: WriteStorage<T>,
    mut windows_data: T::ScreenWindowTypeData,
) {
    let entities = window_system_data.entities;
    let mut screens = window_system_data.screens;
    let active_screen = window_system_data.active_screen;

    if let Some(screen) = active_screen.0 {
        let mut window_entities_to_remove = BitSet::new();

        let window_entities = &screens.get(screen).unwrap().windows;
        for (_window_entity, window) in (window_entities, &mut windows_storage).join() {
            let open = window.ui_show(ctx_ref, &mut windows_data);
            if !open {
                window_entities_to_remove.add(_window_entity);
            }
        }

        for (_id, entity) in (&window_entities_to_remove, &entities).join() {
            entities.delete(entity).unwrap();
        }
        screens
            .get_mut(screen)
            .unwrap()
            .windows
            .bitand_assign(&window_entities_to_remove.not());
    }
}
