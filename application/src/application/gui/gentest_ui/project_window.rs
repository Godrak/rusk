use crate::application::gui::{show_windows, ScreenWindow, WindowSystemData};
use egui::{Id, Ui};
use specs::prelude::*;
use specs::storage::BTreeStorage;
use crate::application::gentest;
use crate::application::core_data::{FetchPromise};
use gentest_api::models::Project;
use instant::Duration;

pub struct ProjectWindow {
    entity: Entity,
    project_idn: String,
    //TODO add promise for project overview, currently useless
}

impl Component for ProjectWindow {
    type Storage = BTreeStorage<Self>;
}

impl ProjectWindow {
    pub fn new(entity: Entity, project_idn: String) -> Self {
        ProjectWindow {
            entity,
            project_idn,
        }
    }

    pub fn name(&self) -> String {
        String::from("📂 ") + self.project_idn.as_str()
    }

    fn show_projects(&mut self, ui: &mut Ui, data: &mut ProjectWindowSystemData) {
        ui.label("NOT IMPLEMENTED");
    }
}

impl<'a> ScreenWindow<'a> for ProjectWindow {
    type ScreenWindowTypeData = ProjectWindowSystemData<'a>;

    /// Show ui and return whether the window was closed (and thus entity should be deleted)
    fn ui_show(&mut self, ctx: &egui::CtxRef, data: &mut Self::ScreenWindowTypeData) -> bool {
        let mut open: bool = true;
        egui::Window::new(self.name())
            .id(Id::new(self.entity))
            .open(&mut open)
            .resizable(true)
            .show(ctx, |ui| {
                self.show_projects(ui, data);
            });
        open
    }
}

#[derive(SystemData)]
pub struct ProjectWindowSystemData<'a> {
    fetcher: Read<'a, super::super::super::core_data::Fetcher>,
}

#[derive(Default)]
pub struct ProjectWindowSystem;

impl<'a> System<'a> for ProjectWindowSystem {
    type SystemData = (
        Read<'a, super::super::GuiContextRes>,
        WindowSystemData<'a>,
        WriteStorage<'a, ProjectWindow>,
        ProjectWindowSystemData<'a>,
    );

    fn run(
        &mut self,
        (context, window_system_data, projects_windows, data): Self::SystemData,
    ) {
        let ctx_ref = context.0.as_ref().unwrap();
        show_windows(ctx_ref, window_system_data, projects_windows, data);
    }
}
