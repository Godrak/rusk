use crate::application::gui::{show_windows, ScreenWindow, WindowSystemData, CreateWindowRequest};
use egui::{Id, Ui};
use specs::prelude::*;
use specs::storage::BTreeStorage;
use crate::application::gentest;
use crate::application::core_data::{FetchPromise};
use gentest_api::models::Project;
use instant::Duration;

pub struct ProjectsWindow {
    entity: Entity,
    projects_list_promise: Option<FetchPromise<gentest::NamesList>>,
    new_project_name: String,
}

impl Component for ProjectsWindow {
    type Storage = BTreeStorage<Self>;
}

impl ProjectsWindow {
    pub fn new(entity: Entity) -> Self {
        ProjectsWindow {
            entity,
            projects_list_promise: None,
            new_project_name: String::default(),
        }
    }

    pub fn name(&self) -> &'static str {
        "📂 Projects"
    }

    fn show_projects(&mut self, ui: &mut Ui, data: &mut ProjectsWindowSystemData) {
        if let Some(promise) = &self.projects_list_promise {
            if promise.is_done() {
                let res = promise.get_result().unwrap();
                if let Ok(names) = res {
                    ui.vertical(|ui| {
                        for name in &names.names {
                            if ui.button(name).clicked()
                            {
                                data.window_creator.create_request(CreateWindowRequest::ProjectWindow(name.clone()));
                            }
                        }
                    });
                } else {
                    ui.label(res.unwrap_err());
                }
            }
        } else {
            self.projects_list_promise = Some(data.fetcher.create_task(
                &|cfg, _| Box::pin(gentest::project_list(cfg)),
                Duration::from_millis(0)));
            ui.label("fetching data");
        }

        ui.horizontal(|ui| {
            ui.label("Project name: ");
            ui.add(egui::TextEdit::singleline(&mut self.new_project_name));
            if ui.button("Create").clicked() {
                let project_name = self.new_project_name.clone();

                data.fetcher.create_task(&|cfg, strvec|
                    {
                        strvec.push(project_name);
                        Box::pin(gentest::project_create(cfg,
                                                       strvec.last().unwrap().as_str(),
                                                       Some(Project { name: strvec.last().unwrap().clone() })))
                    },
                                         Duration::from_millis(0));
                self.new_project_name.clear();
                self.projects_list_promise = Some(data.fetcher.create_task(
                    &|cfg, _| Box::pin(gentest::project_list(cfg)),
                    Duration::from_millis(200)));
            }
        });
    }
}

impl<'a> ScreenWindow<'a> for ProjectsWindow {
    type ScreenWindowTypeData = ProjectsWindowSystemData<'a>;

    /// Show ui and return whether the window was closed (and thus entity should be deleted)
    fn ui_show(&mut self, ctx: &egui::CtxRef, data: &mut Self::ScreenWindowTypeData) -> bool {
        let mut open: bool = true;
        egui::Window::new(self.name())
            .id(Id::new(self.entity))
            .open(&mut open)
            .resizable(true)
            .show(ctx, |ui| {
                self.show_projects(ui, data);
            });
        open
    }
}

#[derive(SystemData)]
pub struct ProjectsWindowSystemData<'a> {
    fetcher: Read<'a, crate::application::core_data::Fetcher>,
    window_creator: Write<'a, super::super::WindowCreatorRes>,
}

#[derive(Default)]
pub struct ProjectsWindowSystem;

impl<'a> System<'a> for ProjectsWindowSystem {
    type SystemData = (
        Read<'a, super::super::GuiContextRes>,
        WindowSystemData<'a>,
        WriteStorage<'a, ProjectsWindow>,
        ProjectsWindowSystemData<'a>,
    );

    fn run(
        &mut self,
        (context, window_system_data, projects_windows, data): Self::SystemData,
    ) {
        let ctx_ref = context.0.as_ref().unwrap();
        show_windows(ctx_ref, window_system_data, projects_windows, data);
    }
}
