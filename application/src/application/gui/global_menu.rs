use egui_wgpu_backend::epi;
use epi::egui;
use specs::prelude::*;

pub struct GlobalMenu {
    main_menu_open: bool,
}

impl Default for GlobalMenu {
    fn default() -> Self {
        GlobalMenu {
            main_menu_open: true,
        }
    }
}

impl GlobalMenu {
    pub fn ui_show(&mut self, ctx: &egui::CtxRef, data: &mut GlobalMenuSystemData) {
        egui::TopBottomPanel::top("top_bar").show(ctx, |ui| {
            egui::trace!(ui);
            self.bar_contents(ui, data);
        });

        if self.main_menu_open || ctx.memory().everything_is_visible() {
            egui::SidePanel::left("main_menu").show(ctx, |ui| {
                self.main_menu_contents(ui, data);
            });
        }
    }

    fn main_menu_contents(&mut self, ui: &mut egui::Ui, data: &mut GlobalMenuSystemData) {
        egui::trace!(ui);
        ui.vertical_centered(|ui| {
            ui.heading("💻 Main Menu");
        });
        ui.separator();

        ui.vertical_centered(|ui| {
            if ui.button("Login").clicked()
            // || (!data.login_data.is_logged_in() && data.login_windows.is_empty()) //TODO makes login necessary
            {
                data.window_creator.create_request(super::CreateWindowRequest::LoginWindow);
            }
            if ui.button("GUI Settings").clicked() {
                data.window_creator.create_request(super::CreateWindowRequest::GuiSettingsWindow);
            }

            if ui.button("Projects").clicked() {
                data.window_creator.create_request(super::CreateWindowRequest::ProjectsWindow);
            }
        });

        ui.separator();
        {
            let mut debug_on_hover = ui.ctx().debug_on_hover();
            ui.checkbox(&mut debug_on_hover, "🐛 Debug on hover")
                .on_hover_text("Show structure of the ui when you hover with the mouse");
            ui.ctx().set_debug_on_hover(debug_on_hover);
        }
    }

    fn bar_contents(&mut self, ui: &mut egui::Ui, data: &mut GlobalMenuSystemData) {
        // A menu-bar is a horizontal layout with some special styles applied.
        ui.horizontal_wrapped(|ui| {
            dark_light_mode_switch(ui);

            ui.checkbox(&mut self.main_menu_open, "💻 Main Menu");
            ui.separator();

            let active_screen_entity = data.active_screen.0;
            for (entity, screen) in (&data.entities, &mut data.screens).join() {
                let screen_label;

                if screen.title_edit_mode {
                    screen_label = ui.text_edit_singleline(&mut screen.title);
                    if screen_label.lost_focus() {
                        screen.title_edit_mode = false;
                    };
                    screen_label.request_focus();
                } else {
                    let checked = active_screen_entity.map(|e| e.eq(&entity)).unwrap_or(false);
                    screen_label = ui.selectable_label(checked, &screen.title);
                    if checked {
                        if ui.small_button("X").clicked() {
                            for (window_entity, _flag) in (&data.entities, &screen.windows).join() {
                                data.entities.delete(window_entity).unwrap();
                            }
                            data.entities.delete(active_screen_entity.unwrap()).unwrap();
                            data.active_screen.0 = None;
                        }
                    }
                }
                if screen_label.clicked() {
                    data.active_screen.0 = Some(entity);
                }
                if screen_label.double_clicked() {
                    screen.title_edit_mode = true;
                }
            }
            ui.separator();
            if ui.button("+").clicked() {
                data.window_creator.create_request(super::CreateWindowRequest::Screen);
            }
        });
    }
}

/// Show a button to switch to/from dark/light mode (globally).
fn dark_light_mode_switch(ui: &mut egui::Ui) {
    let style: egui::Style = (*ui.ctx().style()).clone();
    let new_visuals = style.visuals.light_dark_small_toggle_button(ui);
    if let Some(visuals) = new_visuals {
        ui.ctx().set_visuals(visuals);
    }
}

#[derive(SystemData)]
pub struct GlobalMenuSystemData<'a> {
    pub entities: Entities<'a>,
    pub screens: WriteStorage<'a, super::Screen>,
    pub active_screen: Write<'a, super::ActiveScreen>,
    pub login_data: Read<'a, super::super::core_data::LoginDataRes>,
    pub window_creator: Write<'a, super::WindowCreatorRes>,
}

#[derive(Default)]
pub struct GlobalMenuSystem;

impl<'a> System<'a> for GlobalMenuSystem {
    type SystemData = (
        Read<'a, super::GuiContextRes>,
        Write<'a, super::GlobalMenu>,
        GlobalMenuSystemData<'a>,
    );

    fn run(&mut self, (context, mut global_menu, mut data): Self::SystemData) {
        let ctx_ref = context.0.as_ref().unwrap();
        global_menu.ui_show(ctx_ref, &mut data);
    }
}
