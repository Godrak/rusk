use specs::prelude::*;
use specs::storage::BTreeStorage;

pub struct Screen {
    pub title: String,
    pub windows: BitSet,
    pub title_edit_mode: bool,
}

impl Component for Screen {
    type Storage = BTreeStorage<Self>;
}

#[derive(Default)]
pub struct ActiveScreen(pub Option<Entity>);
