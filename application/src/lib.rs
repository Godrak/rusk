#![cfg(target_arch = "wasm32")]
#![feature(fn_traits)]
#![feature(trait_alias)]

pub use wasm_bindgen_rayon::init_thread_pool;

use raw_window_handle::RawWindowHandle;
use rayon::iter::ParallelIterator;
use rayon::prelude::IntoParallelIterator;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsValue;
use web_sys::OffscreenCanvas;
use winit::dpi::{PhysicalSize, LogicalSize};

pub mod application;
pub mod shared;

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    #[cfg(debug_assertions)]
    console_error_panic_hook::set_once();
    Ok(())
}

struct DummyHandle {}

unsafe impl raw_window_handle::HasRawWindowHandle for DummyHandle {
    fn raw_window_handle(&self) -> RawWindowHandle {
        let mut handle = raw_window_handle::web::WebHandle::empty();
        handle.id = 0;
        raw_window_handle::RawWindowHandle::Web(handle)
    }
}

pub fn global() -> Option<web_sys::WorkerGlobalScope> {
    use wasm_bindgen::JsCast;
    js_sys::global()
        .dyn_into::<web_sys::WorkerGlobalScope>()
        .ok()
}

#[wasm_bindgen]
pub async fn create_app(
    canvas: OffscreenCanvas,
    width: u32,
    height: u32,
    dpi: f32,
) -> AppWebWrapper {
    //Trick to init logger in only one thread
    (0..1).into_par_iter().for_each(|_| {
        let cfg = wasm_logger::Config::default();
        wasm_logger::init(cfg);
    });
    log::info!("logger ready");
    log::info!("application initialization");
    let glob = global().unwrap();
    let logical_size = LogicalSize::new(width, height);
    let physical_size = PhysicalSize::from_logical(logical_size, dpi as f64);
    canvas.set_width(physical_size.width);
    canvas.set_height(physical_size.height);
    log::info!("init width: {} and height: {}", width, height);
    js_sys::Reflect::set(&glob, &JsValue::from_str("canvas"), &canvas).unwrap();
    let app = application::Application::new(&DummyHandle {}, &canvas, physical_size, dpi).await;
    AppWebWrapper { app }
}

#[wasm_bindgen]
pub struct AppWebWrapper {
    app: application::Application,
}

#[wasm_bindgen]
impl AppWebWrapper {
    #[wasm_bindgen]
    pub fn update(&mut self, data: Vec<u8>) {
        let event_in = shared::from_binary(data);
        if event_in.is_some() {
            self.app.handle_event(&event_in.unwrap());
        }
    }

    #[wasm_bindgen]
    pub fn redraw(&mut self) {
        unsafe {
            self.app.handle_event(&shared::WEvent::RedrawRequested(
                winit::window::WindowId::dummy(),
            ));
        }
    }
}
