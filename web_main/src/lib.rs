#![cfg(target_arch = "wasm32")]

use crate::shared::{CustomEvent};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use winit::event::Event::RedrawRequested;
use winit::event::{WindowEvent, VirtualKeyCode};
use winit::event_loop::{ControlFlow, EventLoopProxy};
use std::cmp::min;

pub mod shared;

#[wasm_bindgen]
extern "C" {
    fn handle_event(data: Vec<u8>);
}

async fn paste(proxy: EventLoopProxy<CustomEvent>) {
    let navigator: web_sys::Navigator = web_sys::window().unwrap().navigator();
    #[cfg(web_sys_unstable_apis)]
        let clipboard: web_sys::Clipboard = navigator.clipboard().unwrap();
    let text = wasm_bindgen_futures::JsFuture::from(clipboard.read_text()).await;
    let text = { if text.is_ok() { text.unwrap() } else { text.unwrap_err() } }.as_string().unwrap();
    let mut sent_chars_count = 0;
    while sent_chars_count < text.as_bytes().len() {
        let sendable_text_len = min(text[sent_chars_count..].as_bytes().len(), shared::PASTE_SIZE);
        let mut message = [0; shared::PASTE_SIZE];
        message[..sendable_text_len].copy_from_slice(&text.as_bytes()[sent_chars_count..sent_chars_count + sendable_text_len]);
        proxy.send_event(CustomEvent::Paste(message)).unwrap();
        sent_chars_count += sendable_text_len;
        log::info!("paste event sent")
    }
}

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    #[cfg(debug_assertions)]
        console_error_panic_hook::set_once();
    wasm_logger::init(wasm_logger::Config::default());

    let canvas = web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .get_element_by_id("MainWindow")
        .unwrap()
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .unwrap();

    let event_loop = winit::event_loop::EventLoop::with_user_event();

    use winit::platform::web::WindowBuilderExtWebSys;

    let _window = winit::window::WindowBuilder::new()
        .with_decorations(true)
        .with_transparent(false)
        .with_visible(true)
        .with_title("Rusk")
        .with_canvas(Some(canvas))
        .build(&event_loop)
        .unwrap();

    let proxy = event_loop.create_proxy();
    event_loop.run(move |control_event: shared::WEvent, _, control_flow| {
        *control_flow = ControlFlow::Poll;
        match control_event {
            RedrawRequested(..) => {}
            shared::WEvent::WindowEvent { ref event, .. } => {
                match event {
                    WindowEvent::KeyboardInput { device_id: _, input, is_synthetic: _ } => {
                        //TODO when winit 0.25 starts working on web, we need to track ModifiersChanged instead of this deprecated field
                        if input.modifiers.ctrl() && input.virtual_keycode.map_or(false, |keycode| keycode == VirtualKeyCode::V) {
                            let f = paste(proxy.clone());
                            wasm_bindgen_futures::spawn_local(f);
                        }
                    }
                    WindowEvent::CloseRequested => {
                        *control_flow = ControlFlow::Exit;
                    }
                    _ => {}
                };
                handle_event(shared::to_binary(control_event));
            }
            _ => (handle_event(shared::to_binary(control_event))),
        }
    });
}
