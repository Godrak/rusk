use warp::http::{HeaderMap, HeaderValue};
use warp::Filter;
use arg::Args;
use std::env;


#[derive(Args, Debug)]
///simple server
struct ServerArgs {
    ///Just placeholder
    _exe_name: String,

    #[arg(short, long, default_value = "8000")]
    ///Server port
    port: u32,

    #[arg(required)]
    ///Website directory path
    web_dir: String,

    #[arg(required)]
    ///Certificate Path
    certificate_path: String,

    #[arg(required)]
    ///Key Path
    key_path: String,
}


#[tokio::main]
async fn main() {
    let args : Vec<String> = env::args().collect();
    let args: ServerArgs = Args::from_args(args.iter().map(String::as_str)).unwrap();

    let mut headers = HeaderMap::new();
    headers.insert("Cross-Origin-Opener-Policy", HeaderValue::from_static("same-origin"));
    headers.insert("Cross-Origin-Embedder-Policy", HeaderValue::from_static("require-corp"));

    let web = warp::fs::dir(args.web_dir)
        .with(warp::reply::with::headers(headers));

    warp::serve(web)
        .tls()
        .cert_path(args.certificate_path)
        .key_path(args.key_path)
        .run(([0, 0, 0, 0], 8000)).await;
}