var worker = new Worker('worker.js', {
    type: 'module'
});

console.log("worker created")
var canvas = document.getElementById('MainWindow');

worker.onmessage = function(msg) {
    switch (msg.data.type) {
        case "get_init_data":
            if (!('transferControlToOffscreen' in canvas)) {
                throw new Error('rendering in worker unsupported\n' +
                    'try setting gfx.offscreencanvas.enabled to true in about:config');
            }
            var offscreen = canvas.transferControlToOffscreen();
            worker.postMessage({
                type: "init_data",
                canvas: offscreen,
                width: window.innerWidth,
                height: window.innerHeight,
                dpi: window.devicePixelRatio || 1
            }, [offscreen]);
            break;
    }
};

function handle_event(event) {
    worker.postMessage({
        type: "event",
        data: event,
    });
};

wasm_bindgen('./main/webmain_bg.wasm');
