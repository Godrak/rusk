import init, {
    create_app,
    AppWebWrapper,
    initThreadPool
}
from './application/application.js';
await init('./application/application_bg.wasm');

console.log("initialized application module");

var threads_count = (navigator.hardwareConcurrency || 4);
console.log("init thread pool with " + threads_count + " threads");
await initThreadPool(threads_count);

postMessage({
    type: "get_init_data"
});

var app = null;

addEventListener("message", async function(event) {
    console.log(event);

    var canvas = event.data.canvas;
    var width = event.data.width;
    var height = event.data.height;
    var dpi = event.data.dpi;
    app = await create_app(canvas, width, height, dpi);
    main_loop();
    }, { once: true }
);

function main_loop() {
    onmessage = function(msg) {
        switch (msg.data.type) {
            case "event":
            app.update(msg.data.data);
            break;
        }
    }

    app.redraw();
    requestAnimationFrame(main_loop);
};