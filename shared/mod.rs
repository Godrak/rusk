use abomonation::{decode, encode};

pub(crate) const PASTE_SIZE: usize = 50;

#[derive(Copy, Clone, Debug)]
pub enum CustomEvent {
    Paste([u8; PASTE_SIZE]),
}

pub type WEvent<'a> = winit::event::Event<'a, CustomEvent>;

#[repr(C)]
struct WEventWrap<'a>(WEvent<'a>);

impl abomonation::Abomonation for WEventWrap<'_> {}

pub fn to_binary(event: WEvent) -> Vec<u8> {
    let wrapped = WEventWrap(event);
    let mut bytes = Vec::new();
    unsafe {
        encode(&wrapped, &mut bytes).unwrap();
    }
    bytes
}

pub fn from_binary<'a>(mut data: Vec<u8>) -> Option<WEvent<'a>> {
    if let Some((event, remaining)) = unsafe { decode::<WEventWrap>(&mut data) } {
        assert_eq!(remaining.len(), 0);
        let event = event.0.clone();
        Some(event)
    } else {
        None
    }
}
